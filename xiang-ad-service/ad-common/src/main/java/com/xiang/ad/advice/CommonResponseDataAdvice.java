package com.xiang.ad.advice;

import com.xiang.ad.annotation.IgnoreResponseAdvice;
import com.xiang.ad.vo.CommonResponse;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * Created by xiang.
 */
@RestControllerAdvice // 对响应实现统一拦截
public class CommonResponseDataAdvice implements ResponseBodyAdvice<Object> {

    @Override
    @SuppressWarnings("all")//isAnnotationPresent方法，有可能会产生空指针异常
    // 根据一些判断条件去判断，响应是否应该拦截，是否支持拦截，返回值是boolean
    public boolean supports(MethodParameter methodParameter,
                            Class<? extends HttpMessageConverter<?>> aClass) {
        // 通过methodParameter拿到类的声明，如果这个类的声明被（传入）这个注解标识，那么就返回false
        if (methodParameter.getDeclaringClass().isAnnotationPresent(
                IgnoreResponseAdvice.class
        )) {
            return false;
        }
        // 方法级别，同上
        if (methodParameter.getMethod().isAnnotationPresent(
                IgnoreResponseAdvice.class
        )) {
            return false;
        }
        return true; //需要做统一响应的话，就返回true，进行下面方法的处理
    }

    @Nullable
    @Override
    @SuppressWarnings("all")
    // 在写入响应之前可以做一些操作，完成CommonResponse统一拦截就是在这里
    // 生成CommonResponse，返回给用户，用户最终拿到的就是CommonResponse
    public Object beforeBodyWrite(@Nullable Object o, //o是一个响应对象
                                  MethodParameter methodParameter,
                                  MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        //定义最终的返回对象
        CommonResponse<Object> response = new CommonResponse<>(0, "");
        // 如果 o 是 null, response 不需要设置 data
        if (null == o) {
            return response;
        } else if (o instanceof CommonResponse) { // 如果已经是CommonResponse，就不需再进行处理了，多加一层处理可能会出现错误
            response = (CommonResponse<Object>) o;//如果 o 已经是 CommonResponse 类型, 强转即可
        } else {
            response.setData(o);//否则, 把响应对象作为 CommonResponse 的 data 部分
        }

        return response;
    }
}
