package com.xiang.ad.exception;

/**
 * Created by xiang.
 */
// 自定义统一的异常处理类
public class AdException extends Exception {

    public AdException(String message) {
        super(message);//直接使用Exception的构造函数
    }
}
