package com.xiang.ad.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Created by xiang.
 */
@Configuration  // 这个WebConfiguration需要被扫描到
public class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>>
                                                       converters) {//converters说明springboot提供给我们的是多个转换器

        converters.clear(); // 对转换器列表清空
        //MappingJackson2HttpMessageConverter可以实现将 java对象转换成json对象
        converters.add(new MappingJackson2HttpMessageConverter()); //只使用一个转换器
    }
}