package com.xiang.ad.advice;

import com.xiang.ad.exception.AdException;
import com.xiang.ad.vo.CommonResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by xiang.
 * 全局异常处理
 * RestControllerAdvice: 组合注解, ControllerAdvice + ResponseBody, 是对 RestController 的功能增强
 * 当我们在代码中主动抛出来异常，就会被这个GlobalExceptionAdvice去捕获到
 * 进而通过handlerAdException这个方法进行异常处理。
 */
@RestControllerAdvice  // 异常处理也是一种响应,也需要这个注释来拦截响应
public class GlobalExceptionAdvice {

    /**
     * ExceptionHandler: 可以对指定的异常进行拦截
     * 优化: 定义多种类异常, 并实现对应的异常处理. 例如: 推广单元的操作出现异常, 抛出 AdUnitException;
     *  Binlog 解析异常, 抛出 BinlogException
     * */
    //value 是一个 Class 数组，用于指定需要拦截的异常类。
    @ExceptionHandler(value = AdException.class) // 标识只处理一种异常AdException，否则会处理所有的异常
    // 同样使用CommonResponse，即使是异常，也要统一响应
    // 一旦发生异常，就会传进来俩参数 HttpServletRequest、AdException
    public CommonResponse<String> handlerAdException(HttpServletRequest req, AdException ex) {
        // 统一异常接口的响应
        // 优化: 定义不同类型的异常枚举(异常码和异常信息)
        CommonResponse<String> response = new CommonResponse<>(-1,
                "business error");
        response.setData(ex.getMessage());//展现出来是哪些错误抛出的异常，异常的信息是什么
        return response;
    }
}
