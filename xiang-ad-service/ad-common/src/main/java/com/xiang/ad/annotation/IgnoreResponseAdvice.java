package com.xiang.ad.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by xiang.
 */
@Target({ElementType.TYPE, ElementType.METHOD})  // 可以标识在类class上，也可以表示在方法上
@Retention(RetentionPolicy.RUNTIME) // 选择 运行时retention
public @interface IgnoreResponseAdvice {
}
