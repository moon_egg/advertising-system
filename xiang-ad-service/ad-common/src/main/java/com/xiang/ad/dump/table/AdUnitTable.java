package com.xiang.ad.dump.table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 推广单元 所需要的 字段说明
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitTable {

    private Long unitId;//推广单元的id
    private Integer unitStatus;//推广单元状态
    private Integer positionType;

    private Long planId;
}
