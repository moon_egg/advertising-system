package com.xiang.ad.dump.table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 创意表 所需要的 字段属性说明
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdCreativeTable {

    private Long adId;//创意id  就是广告id
    private String name;// 创意的名称
    private Integer type;//创意类型
    private Integer materialType;//创意子类型
    private Integer height;//高度
    private Integer width;//宽度
    private Integer auditStatus;//审核状态
    private String adUrl;//链接
}
