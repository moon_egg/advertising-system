package com.xiang.ad.dump.table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by xiang.
 * 推广计划表 所需要的 字段属性说明
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdPlanTable {

    private Long id; //推广计划ad
    private Long userId; //推广计划 关联的 广告主id
    private Integer planStatus; //推广计划状态
    private Date startDate; //开始时间
    private Date endDate; //结束时间
}
