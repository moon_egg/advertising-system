package com.xiang.ad.dump;

/**
 * Created by xiang.
 * 常量的文件声明
 * 将广告数据的mysql数据导出到文件的目录名和文件名
 */

//1、mysql数据导出到指定目录
//2、数据表对应的存储文件的名字
public class DConstant {

    // 存储目录
    public static final String DATA_ROOT_DIR = "D:\\idea\\project\\xiang-ad\\mysql_data\\";

    // 各个表数据的存储文件名
    public static final String AD_PLAN = "ad_plan.data";
    public static final String AD_UNIT = "ad_unit.data";
    public static final String AD_CREATIVE = "ad_creative.data";

    public static final String AD_CREATIVE_UNIT = "ad_creative_unit.data";//创意与推广单元关联关系的数据

    public static final String AD_UNIT_KEYWORD = "ad_unit_keyword.data";
    public static final String AD_UNIT_IT = "ad_unit_it.data";
    public static final String AD_UNIT_DISTRICT = "ad_unit_district.data";

}
