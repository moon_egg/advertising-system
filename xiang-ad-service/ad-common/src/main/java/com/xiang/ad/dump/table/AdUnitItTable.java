package com.xiang.ad.dump.table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 推广单元 兴趣限制表 所需要的 字段属性说明
 * 之后导出的文件 就会以这里面的属性为基础 去导出这些表数据的属性，定义了哪些属性，就需要导出表里包含的哪些属性
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitItTable {

    private Long unitId;//推广单元id
    private String itTag;//兴趣标签
}
