package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by xiang.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
// 统一响应，最重要的是有一个统一的结构，因为不知道具体是什么类型， 所以用泛型来指定，可以是任意类型
public class CommonResponse<T> implements Serializable {

    private Integer code; //响应码
    private String message; // 响应信息
    private T data; //响应体

    // 没有data的构造函数，因为data最终是什么还需要根据具体的业务逻辑去相应
    public CommonResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
