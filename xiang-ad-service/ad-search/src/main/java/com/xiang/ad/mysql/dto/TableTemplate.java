package com.xiang.ad.mysql.dto;

import com.xiang.ad.mysql.constant.OpType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xiang.
 * 对导出文件的数据表，方便读取一些表的信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TableTemplate {

    private String tableName; //table的name
    private String level;

    //对表的 操作类型-字段   封装成map
    private Map<OpType, List<String>> opTypeFieldSetMap = new HashMap<>();

    /**
     * positionMap
     * 字段索引 -> 字段名 的映射
     * 因为Binlog中不会列出来具体的修改的列的名子，列出来的都是索引
     * 需要根据具体的索引映射到字段的名字
     * */
    private Map<Integer, String> posMap = new HashMap<>();
}
