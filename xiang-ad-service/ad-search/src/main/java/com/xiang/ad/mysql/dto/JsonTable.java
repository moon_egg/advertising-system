package com.xiang.ad.mysql.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * java对象去表达template.json模板文件(解析Binlog的基础)  对表的表达
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonTable {

    private String tableName; //table的名字
    private Integer level; //层级

    private List<Column> insert;  //
    private List<Column> update;  //
    private List<Column> delete;  //

    //
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Column {

        private String column;
    }
}
