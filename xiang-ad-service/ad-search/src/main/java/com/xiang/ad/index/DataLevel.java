package com.xiang.ad.index;

import lombok.Getter;

/**
 * Created by xiang.
 * 构建索引的时候，是根据层级关系（表之间的关系）去构件的，这里需要常量定义，定义这个关系
 * 第二层级代表着：不与其他层级存在着关联关系，不依赖于其他的索引就可以构建
 * 第三层级索引代表着：依赖于第二层级的plan，并且与自身（第三层级）相关联
 * 第四层级索引代表着：三个维度的限制都与第三层级的unit有依赖关系
 */
@Getter
public enum DataLevel {

    LEVEL2("2", "level 2"),//第二层级：plan、creative
    LEVEL3("3", "level 3"),//第三层级：unit
    LEVEL4("4", "level 4");//第四层级：三个限制

    private String level; //层级
    private String desc;  //对层级的描述信息

    DataLevel(String level, String desc) {
        this.level = level;
        this.desc = desc;
    }
}
