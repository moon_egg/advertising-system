package com.xiang.ad.controller;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.annotation.IgnoreResponseAdvice;
import com.xiang.ad.client.SponsorClient;
import com.xiang.ad.client.SponsorClientHystrix;
import com.xiang.ad.client.vo.AdPlan;
import com.xiang.ad.client.vo.AdPlanGetRequest;

import com.xiang.ad.search.ISearch;
import com.xiang.ad.search.vo.SearchRequest;
import com.xiang.ad.search.vo.SearchResponse;
import com.xiang.ad.vo.CommonResponse;
import io.micrometer.core.instrument.search.Search;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by xiang.
 * 使用ribbon调用
 */
@Slf4j
@RestController
public class SearchController {


    //检索服务 查询响应
    private final ISearch search;

    //为ribbon使用
    private final RestTemplate restTemplate;

    //注入feign的 SponsorClient
    private final SponsorClient sponsorClient;


    @Autowired
    public SearchController(RestTemplate restTemplate,
                            SponsorClient sponsorClient ,
                            ISearch search) { //idea辣鸡
        this.restTemplate = restTemplate;
        this.sponsorClient = sponsorClient;
        this.search = search;
    }

    //检索请求
    @PostMapping("/fetchAds")
    public SearchResponse fetchAds(@RequestBody SearchRequest request) {//RequestBody Json到java的反序列化

        log.info("ad-search: fetchAds -> {}",
                JSON.toJSONString(request));
          return search.fetchAds(request);//广告的检索请求的服务接口
    }

    //使用feigh的方式 获取 adPlan
    @IgnoreResponseAdvice //不需要再包一层
    @PostMapping("/getAdPlans")
    public CommonResponse<List<AdPlan>> getAdPlans(
            @RequestBody AdPlanGetRequest request
    ) {
        log.info("ad-search: getAdPlans -> {}",
                JSON.toJSONString(request));
        //使用feigh的当时 获取adPlan
        return sponsorClient.getAdPlans(request);
    }


    //=================使用ribbon的方式调用服务（测试用例，项目中用的是feigh）====================
    @SuppressWarnings("all")
    @IgnoreResponseAdvice //不想使用统一的响应
    @PostMapping("/getAdPlansByRibbon")
    public CommonResponse<List<AdPlan>> getAdPlansByRibbon(
            @RequestBody AdPlanGetRequest request
    ) {
        log.info("ad-search: getAdPlansByRibbon -> {}",
                JSON.toJSONString(request));
        return restTemplate.postForEntity(
                //eureka-client-ad-sponsor 是投放系统的应用名称,ribbon看到这个会去eureka server中拿到这个投放系统的元信息
                //然后调用后面的方法ad-sponsor/get/adPlan
                //request是参数
                //CommonResponse是提供序列化响应的格式
                "http://eureka-client-ad-sponsor/ad-sponsor/get/adPlan",
                request,
                CommonResponse.class
        ).getBody();
    }
}
