package com.xiang.ad.mysql.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 对模板文件template.json 的整个模板文件的表达
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Template {

    private String database;  //当前只对一个数据库进行解析
    private List<JsonTable> tableList;  //对应
}
