package com.xiang.ad.runner;

import com.xiang.ad.mysql.BinlogClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by xiang.
 * 程序启动的时候，自动运行的程序
 */
@Slf4j
@Component
//程序启动的时候就应该开始监听  实现CommandLineRunner接口，或者applicationRunner
public class BinlogRunner implements CommandLineRunner {

    //注入Binlog客户端
    private final BinlogClient client;

    @Autowired
    public BinlogRunner(BinlogClient client) {
        this.client = client;
    }

    //当应用程序启动的时候，就开启binlog的监听，将应用程序伪装成mysql的slave实现监听
    //在connect方法中，新建了一个线程，线程中初始化一个client
    //然后定义了client的监听器，根据配置去定义filename以及position
    //最后调用了connect方法，实现了监听
    @Override
    public void run(String... strings) throws Exception {

        log.info("准备开启 BinlogRunner...");
        client.connect();//启动binlog客户端
    }
}
