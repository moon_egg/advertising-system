package com.xiang.ad.index.adunit;

/**
 * Created by xiang.
 * 流量类型-用于根据流量类型实现对推广单元的预筛选
 */
public class AdUnitConstants {

    //流量类型，这个POSITION_TYPE是广告术语里面的，不是地理位置
    public static class POSITION_TYPE {

        //使用二进制存储，方便位或运算
        public static final int KAIPING = 1;//开屏
        public static final int TIEPIAN = 2;//贴片
        public static final int TIEPIAN_MIDDLE = 4;//中贴
        public static final int TIEPIAN_PAUSE = 8;//暂停贴
        public static final int TIEPIAN_POST = 16;//后贴，视频播放完展示一些广告
    }
}
