package com.xiang.ad.index;

import lombok.Getter;

/**
 * Created by xiang.
 * 定义枚举类，标记推广单元和推广计划的状态，用于检索时候的判断
 */
@Getter
public enum CommonStatus {

    VALID(1, "有效状态"),
    INVALID(0, "无效状态");

    private Integer status;//状态
    private String desc;//描述信息

    CommonStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }
}
