package com.xiang.ad.mysql.constant;

import com.github.shyiko.mysql.binlog.event.EventType;

/**
 * Created by xiang.
 * 为构建索引提供一些 操作类型
 * 这里实现 BinlogRowData中的 eventtype(开源工具中的类型)到 MysqlRowData中的 opType的转换
 */
public enum OpType {

    ADD,    //添加
    UPDATE, //更新
    DELETE, //删除
    OTHER;  //其他操作

    //实现 BinlogRowData中的 eventtype到 MysqlRowData中的 optype的转换
    public static OpType to(EventType eventType) {

        //关心的只有三类  增  删  更新
        switch (eventType) {
            case EXT_WRITE_ROWS:
                return ADD;
            case EXT_UPDATE_ROWS:
                return UPDATE;
            case EXT_DELETE_ROWS:
                return DELETE;

            default:
                return OTHER;
        }
    }
}
