package com.xiang.ad.sender;

import com.xiang.ad.mysql.dto.MySqlRowData;

/**
 * Created by xiang.
 * 投递增量数据的 接口
 */
public interface ISender {

    //投递
    void sender(MySqlRowData rowData);
}
