package com.xiang.ad.mysql.dto;

import com.xiang.ad.mysql.constant.OpType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xiang.
 * 为检索服务要实现的增量数据的投递 需要的对象：MySqlRowData
 * 投递对象
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MySqlRowData {

    //目前我只有一个数据库，就不定义数据库了，将来扩展为多数据库的话，就再重构定义一个字段

    private String tableName;//表名

    private String level;//数据表层级关系（检索业务中会用到）

    private OpType opType;//操作类型  增-删-改

    //after部分
    private List<Map<String, String>> fieldValueMap = new ArrayList<>();
}
