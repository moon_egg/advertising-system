package com.xiang.ad.client;

import com.xiang.ad.client.vo.AdPlan;
import com.xiang.ad.client.vo.AdPlanGetRequest;
import com.xiang.ad.vo.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by xiang.
 * 使用feign定义接口
 */
//@Component  问了不用加，但是不加 idea在controller层就报错，估计是不识别
@FeignClient(value = "eureka-client-ad-sponsor", //指向微服务的应用名称
        fallback = SponsorClientHystrix.class) //服务降级
public interface SponsorClient {


    //获取adPlan
    @RequestMapping(value = "/ad-sponsor/get/adPlan",//指定对应应用的 哪一个服务接口
            method = RequestMethod.POST)
    CommonResponse<List<AdPlan>> getAdPlans(
            @RequestBody AdPlanGetRequest request);

}
