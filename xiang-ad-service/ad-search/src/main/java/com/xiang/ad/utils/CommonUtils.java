package com.xiang.ad.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created by xiang.
 */
@Slf4j
public class CommonUtils {

    /**
     *
     * @param key
     * @param map
     * @param factory
     * @param <K>
     * @param <V>
     * @return
     * 传递进来map，如果这个map的key不存在的情况下（computeIfAbsent），
     * computeIfAbsent方法是以第一个参数作为key查询value，如果查询失败，则将第二个参数的返回值作为value存入map并返回
     * 使用factory去返回一个新的对象，
     */
    public static <K, V> V getorCreate(K key,
                                       Map<K, V> map,
                                       Supplier<V> factory) {
        return map.computeIfAbsent(key, k -> factory.get());
        /**
         *   computeIfAbsent 方法有两个参数，第一个参数是想要获取的 map 的 key，获取不到的时候才会执行第二个参数指向的表达式。
         *   所以，当 map 存在 key，是不会创建新的 map 的；
         */
    }

    //实现字符串之间通过连接符进行拼接
    public static String stringConcat(String... args) {

        StringBuilder result = new StringBuilder();
        for (String arg : args) {
            result.append(arg);
            result.append("-");
        }
        result.deleteCharAt(result.length() - 1);//删掉末尾的 -
        return result.toString();
    }

    // 解析时间
    // Binlog中的date形式： Fri Aug 20 07:09:22 CST 2021
    public static Date parseStringDate(String dateString) {

        try {
            //星期几-月-天-小时-分钟-秒 时间类型  年
            DateFormat dateFormat = new SimpleDateFormat(
                    "EEE MMM dd HH:mm:ss zzz yyyy",
                    Locale.US  //CST 美国时间，默认比中国地区时间多8个小时
            );
            return DateUtils.addHours(
                    dateFormat.parse(dateString),  //apache的工具类
                    -8  //-8才是我们数据库操作的时间
            );

        } catch (ParseException ex) {
            log.error("parseStringDate error: {}", dateString);//可能会出错，打一下日志看看
            return null;
        }
    }
}
