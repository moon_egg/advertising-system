package com.xiang.ad.search.vo.feature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 匹配信息对象的定义---关键词
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeywordFeature {

    //关键词
    private List<String> keywords;
}
