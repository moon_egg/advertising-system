package com.xiang.ad.client;

import com.xiang.ad.client.vo.AdPlan;
import com.xiang.ad.client.vo.AdPlanGetRequest;
import com.xiang.ad.vo.CommonResponse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by xiang.
 * 断位器
 */
@Component //声明为一个组件
public class SponsorClientHystrix implements SponsorClient {

    @Override
    public CommonResponse<List<AdPlan>> getAdPlans(
            AdPlanGetRequest request) {
        return new CommonResponse<>(-1,
                "eureka-client-ad-sponsor error");
    }
}
