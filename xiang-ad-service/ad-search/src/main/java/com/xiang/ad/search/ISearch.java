package com.xiang.ad.search;

import com.xiang.ad.search.vo.SearchRequest;
import com.xiang.ad.search.vo.SearchResponse;

/**
 * Created by xiang.
 * 广告的检索请求的服务接口
 * 根据广告请求 获得 广告响应
 */
public interface ISearch {

    //请求的响应方法，具体我在SearchImpl中实现了
    SearchResponse fetchAds(SearchRequest request);
}
