package com.xiang.ad.search.vo.feature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 匹配信息对象的定义---地域限制
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistrictFeature {

    //地域限制list，里面装 封装好的内部类
    private List<ProvinceAndCity> districts;

    //内部类，定义省和市
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProvinceAndCity {

        private String province;//省
        private String city;//市
    }
}
