package com.xiang.ad.index.adplan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by xiang.
 * adPlan的基本信息 及 更新方法
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdPlanObject {

    private Long planId; //计划id
    private Long userId; //关联的用户id
    private Integer planStatus; //推广计划的状态
    private Date startDate; //计划开始时间
    private Date endDate; //计划结束时间

    //更新索引的时候  会用到这个方法，不一定会需要所有字段，非空的时候才更新对应的字段
    public void update(AdPlanObject newObject) {

        if (null != newObject.getPlanId()) {
            this.planId = newObject.getPlanId();
        }
        if (null != newObject.getUserId()) {
            this.userId = newObject.getUserId();
        }
        if (null != newObject.getPlanStatus()) {
            this.planStatus = newObject.getPlanStatus();
        }
        if (null != newObject.getStartDate()) {
            this.startDate = newObject.getStartDate();
        }
        if (null != newObject.getEndDate()) {
            this.endDate = newObject.getEndDate();
        }
    }
}
