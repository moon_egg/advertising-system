package com.xiang.ad.search.vo;

import com.xiang.ad.search.vo.feature.DistrictFeature;
import com.xiang.ad.search.vo.feature.FeatureRelation;
import com.xiang.ad.search.vo.feature.ItFeature;
import com.xiang.ad.search.vo.feature.KeywordFeature;
import com.xiang.ad.search.vo.media.AdSlot;
import com.xiang.ad.search.vo.media.App;
import com.xiang.ad.search.vo.media.Device;
import com.xiang.ad.search.vo.media.Geo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 检索服务 请求对象
 * 请求方的VO对象：我这里只给信息设了三个部分，以后还可以扩展
 * 1、唯一标识ID
 * 2、请求对象的信息，这里的请求对象是媒体方
 * 3、匹配信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequest {

    // 媒体方的请求标识
    private String mediaId;
    // 一次请求包含的基本信息
    private RequestInfo requestInfo;
    // 匹配信息
    private FeatureInfo featureInfo;

    //请求对象信息---媒体方的信息
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RequestInfo {

        //唯一的请求ID
        private String requestId;

        //媒体方信息
        private List<AdSlot> adSlots;//广告位
        private App app;//app的终端信息
        private Geo geo;//媒体方地理信息
        private Device device;//设备信息
    }

    //匹配信息封装
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FeatureInfo {

        private KeywordFeature keywordFeature;//关键词匹配
        private DistrictFeature districtFeature;//地域匹配
        private ItFeature itFeature;//兴趣匹配

        //默认如果不填就是and的关系，就是必须全部满足才可以返回 检索请求的结果
        private FeatureRelation relation = FeatureRelation.AND;
    }
}
