package com.xiang.ad.index.district;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 地域维度index对象
 * 推广单元的限制维度  使用倒排索引，以地域的形式寻找推广单元的id，一个地域可以对应到多个推广单元的id
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitDistrictObject {

    private Long unitId; // 与推广单元关联的id
    private String province; // 省
    private String city; //  市

    // 构造索引的时候，将省和市连起来，够造成一个字段
    // <String, Set<Long>>
    // province-city
}
