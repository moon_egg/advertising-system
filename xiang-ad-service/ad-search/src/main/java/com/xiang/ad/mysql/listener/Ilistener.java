package com.xiang.ad.mysql.listener;

import com.xiang.ad.mysql.dto.BinlogRowData;

/**
 * Created by xiang.
 * 实现对binlog日志数据实现增量索引更新的接口
 */
public interface Ilistener {

    //因为需要对不同的表定义不同的增量数据的更新方法
    void register();//注册不同的监听器

    //实现增量数据索引的更新
    void onEvent(BinlogRowData eventData);
}
