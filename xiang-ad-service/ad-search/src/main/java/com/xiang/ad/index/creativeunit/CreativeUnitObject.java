package com.xiang.ad.index.creativeunit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 创意&推广单元 index实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeUnitObject {

    private Long adId; //创意对象id  就是广告id
    private Long unitId; //推广单元id


    // adId-unitId结合在一起作为key
}
