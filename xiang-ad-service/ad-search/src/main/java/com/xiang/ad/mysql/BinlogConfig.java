package com.xiang.ad.mysql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by xiang.
 * 将配置实现初始化
 */
@Component
@ConfigurationProperties(prefix = "adconf.mysql") //实现配置文件到Java对象的转化，对应application.yml中的adconf.mysql
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BinlogConfig {

    private String host;//主机名
    private Integer port;//端口号
    private String username;
    private String password;

    private String binlogName;
    private Long position;
}
