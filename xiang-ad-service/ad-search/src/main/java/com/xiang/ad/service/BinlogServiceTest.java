package com.xiang.ad.service;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

import java.util.HashMap;

/**
 * Created by xiang.
 * 实现对 MySQL 数据表状态的变更有所感知
 * 也就是能够实现对 Binlog 的监听、并解析成我们 ”想要的格式（Java 对象）”。
 */
public class BinlogServiceTest {

    public static void main(String[] args) throws Exception {

        //BinaryLogClient就是连接数据库的客户端，把自己作为slave连接到mysql（master）上
        BinaryLogClient client = new BinaryLogClient(
                "127.0.0.1",
                3306,
                "root",
                "root"
        );
        client.setBinlogFilename("DESKTOP-FG0I4KK-bin.001468");
        // DESKTOP-FG0I4KK-bin.001468
        // 可以设定读取 Binlog 的文件和位置，那么，client 将从这个位置之后开始监听
        // 否则, client 会从 “头” 开始读取 Binlog 文件，并监听
        // client.setBinlogFilename("binlog.000037");
        // client.setBinlogPosition();


        // 注册事件监听器  这里做测试用
        client.registerEventListener(event -> {

            EventData data = event.getData();

            if (data instanceof UpdateRowsEventData) { //如果是更新数据的event
                System.out.println("Update--------------");
                System.out.println(data.toString());
            } else if (data instanceof WriteRowsEventData) {//如果是写数据的event
                System.out.println("Write---------------");
                System.out.println(data.toString());
            } else if (data instanceof DeleteRowsEventData) {//如果是删除数据的event
                System.out.println("Delete--------------");
                System.out.println(data.toString());
            }
        });

        client.connect();
    }
}

/*
 * 测试结果如下：
 * tableId：数据表的id
 * includedColumns：发生变化的这些列  0 1 2 并不是列的名字，是mysql底层给数据表每一列维护的索引
 * rows：列表，包含了刚刚写入的数据
 =============插入================
 当我给ad_unit_keyword插入了一行数据后
 Write---------------
 WriteRowsEventData{tableId=94, includedColumns={0, 1, 2}, rows=[
 [1, 10, 测试用]
 ]}
 =============更新================
 Update--------------
 UpdateRowsEventData{tableId=94, includedColumnsBeforeUpdate={0, 1, 2}, includedColumns={0, 1, 2}, rows=[
 {before=[1, 10, 测试用], after=[1, 11, 测试用]}
 ]}
 =============删除================
 Delete--------------
 DeleteRowsEventData{tableId=94, includedColumns={0, 1, 2}, rows=[
 [1, 11, 测试用]
 ]}
 *
 * Fri Aug 20 07:09:22 CST 2021
 */
