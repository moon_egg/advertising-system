package com.xiang.ad.index.creative;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 创意 index对象
 * 这个创意object只有一个正向索引，也就是通过id获取CreativeObject
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeObject {

    private Long adId; // 创意id，也就是广告id
    private String name;// 创意的名称
    private Integer type;// 创意的类型 图片、视频、文本
    private Integer materialType;//子类型 jpg、png、、、
    private Integer height;// 高度
    private Integer width;// 宽度
    private Integer auditStatus; // 审核状态
    private String adUrl;// 广告的url

    //提供更新操作，非空才对应更新
    public void update(CreativeObject newObject) {

        if (null != newObject.getAdId()) {
            this.adId = newObject.getAdId();
        }
        if (null != newObject.getName()) {
            this.name = newObject.getName();
        }
        if (null != newObject.getType()) {
            this.type = newObject.getType();
        }
        if (null != newObject.getMaterialType()) {
            this.materialType = newObject.getMaterialType();
        }
        if (null != newObject.getHeight()) {
            this.height = newObject.getHeight();
        }
        if (null != newObject.getWidth()) {
            this.width = newObject.getWidth();
        }
        if (null != newObject.getAuditStatus()) {
            this.auditStatus = newObject.getAuditStatus();
        }
        if (null != newObject.getAdUrl()) {
            this.adUrl = newObject.getAdUrl();
        }
    }

}
