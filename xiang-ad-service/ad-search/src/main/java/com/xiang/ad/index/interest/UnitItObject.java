package com.xiang.ad.index.interest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 兴趣-索引对象
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitItObject {

    private Long unitId;// 与推广单元的关联id，以外键的形式存在
    private String itTag;// 兴趣的tag
}
