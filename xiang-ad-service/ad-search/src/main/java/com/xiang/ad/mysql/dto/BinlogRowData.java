package com.xiang.ad.mysql.dto;

import com.github.shyiko.mysql.binlog.event.EventType;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created by xiang.
 * 实现event到binlog的映射
 * 这里数据是冗余的，为了以后扩展方便
 * 1、这里的数据简化为 为了投递工作去构建增量索引
 * 2、（其他处理功能还未设计）
 */
@Data
public class BinlogRowData {

    private TableTemplate table;//表模板

    private EventType eventType;//操作类型（开源工具中的类型）

    //更新 插入 删除 都有after
    private List<Map<String, String>> after;//删除（更新）时候的after

    //只有删除才有before
    private List<Map<String, String>> before;//删除时候的before
}
