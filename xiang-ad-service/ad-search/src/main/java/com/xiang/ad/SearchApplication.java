package com.xiang.ad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by xiang.
 * 启动类
 */
@EnableFeignClients  //使用feigh访问其他微服务
@EnableEurekaClient  //作为一个EurekaClient存在
@EnableHystrix       //断路器
@EnableCircuitBreaker  //断路器
@EnableDiscoveryClient //开启微服务的发现能力，以便ribbon可以访问到这个微服务
@EnableHystrixDashboard //Hystrix监控
@SpringBootApplication
public class SearchApplication {

    public static void main(String[] args) {

        SpringApplication.run(SearchApplication.class, args);
    }

    //定义rest客户端，为了ribbon调用使用
    @Bean
    @LoadBalanced // 负载均衡的能力   如果有多个部署的广告投放实例，可以实现轮询
    RestTemplate restTemplate() {

        return new RestTemplate();
    }
}
