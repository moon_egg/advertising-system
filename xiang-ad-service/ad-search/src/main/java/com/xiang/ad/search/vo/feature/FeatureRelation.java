package com.xiang.ad.search.vo.feature;

/**
 * Created by xiang.
 * 匹配信息对象的定义---匹配信息之间的关系
 */
public enum FeatureRelation {

    OR,//只要有一个匹配就能返回
    AND//全部匹配才能返回
}
