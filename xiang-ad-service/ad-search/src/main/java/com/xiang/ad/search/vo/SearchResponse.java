package com.xiang.ad.search.vo;

import com.xiang.ad.index.creative.CreativeObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xiang.
 * 检索服务 响应对象
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchResponse {

    //response中包含的内容，对应到每一个adSlotCode都会返回广告创意数据
    // List<Creative> 就是装着检索来的对象信息
    public Map<String, List<Creative>> adSlot2Ads = new HashMap<>();

    //返回的创意
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Creative {

        private Long adId;//ID
        private String adUrl;//广告的URL，媒体方（请求方也是接收方）利用这个url下载广告数据
        private Integer width;//宽度
        private Integer height;//高度
        private Integer type;//广告的类型，媒体方才能选择对应的decoder进行数据的解码
        private Integer materialType;//子类型，物料类型，就是广告里面的 什么 贴屏啥的

        /**
         * 当广告返回给媒体方之后，媒体方会对广告进行曝光啥的，就是展示出来。
         * 一旦展示，就调用下面两个方法，检测和跟踪这个过程（谁展示了，谁点击了）
         * 我这里是考虑到对数据的跟踪，打算统计下数量啥的，以后根据算法优化广告库存之类的。
         * 不知道咋实现呢，先实现下预想。
         */

        // 展示监测 url，
        private List<String> showMonitorUrl =
                Arrays.asList("www.xiang.com", "www.xiang.com");
        // 点击监测 url
        private List<String> clickMonitorUrl =
                Arrays.asList("www.xiang.com", "www.xiang.com");
    }

    // 检索最终得到的是检索对象
    // 将索引对象 转换为 定义的返回给用户的对象
    public static Creative convert(CreativeObject object) {

        //对应字段的转换
        Creative creative = new Creative();
        creative.setAdId(object.getAdId());
        creative.setAdUrl(object.getAdUrl());
        creative.setWidth(object.getWidth());
        creative.setHeight(object.getHeight());
        creative.setType(object.getType());
        creative.setMaterialType(object.getMaterialType());

        return creative;
    }
}
