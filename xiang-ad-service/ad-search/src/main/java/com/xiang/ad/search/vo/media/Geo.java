package com.xiang.ad.search.vo.media;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 媒体的基本信息---地理位置信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Geo {

    private Float latitude;//维度
    private Float longitude;//经度

    private String city;//城市
    private String province;//省份
}
