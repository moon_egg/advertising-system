package com.xiang.ad.index;

/**
 * Created by xiang.
 * index操作接口      get、add、update、delete
 */
// 接收 kv 泛型值，K代表键，V代表值
public interface IndexAware<K, V> {


    V get(K key);//获取value

    void add(K key, V value); //添加index

    void update(K key, V value); //更新索引

    void delete(K key, V value); //删除索引
}
