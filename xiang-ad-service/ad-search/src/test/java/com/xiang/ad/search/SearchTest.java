package com.xiang.ad.search;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.Application;
import com.xiang.ad.search.vo.SearchRequest;
import com.xiang.ad.search.vo.feature.DistrictFeature;
import com.xiang.ad.search.vo.feature.FeatureRelation;
import com.xiang.ad.search.vo.feature.ItFeature;
import com.xiang.ad.search.vo.feature.KeywordFeature;
import com.xiang.ad.search.vo.media.AdSlot;
import com.xiang.ad.search.vo.media.App;
import com.xiang.ad.search.vo.media.Device;
import com.xiang.ad.search.vo.media.Geo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by xiang.
 * 测试可以看到两段信息
 * 1、就是各种index的 before啥的，那个是之前就构建好的，检索服务启动的时候，加载了全量索引
 */
@RunWith(SpringRunner.class) //标记为测试用例
@SpringBootTest(classes = {Application.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE)  //不需要web环境
public class SearchTest {

    //注入检索服务接口
    @Autowired
    private ISearch search;


    //这里测试 （现在只有这个接口）检索服务
    @Test
    public void testFetchAds() {

        SearchRequest request = new SearchRequest();
        request.setMediaId("xiang-ad");

        // 第一个测试条件
        request.setRequestInfo(new SearchRequest.RequestInfo(
                "aaa",
                Collections.singletonList(new AdSlot(
                        "ad-x", 1,
                        1080, 720, Arrays.asList(1, 2),
                        1000
                )),
                buildExampleApp(),
                buildExampleGeo(),
                buildExampleDevice()
        ));
        request.setFeatureInfo(buildExampleFeatureInfo(
                Arrays.asList("宝马", "大众"),
                Collections.singletonList(
                        new DistrictFeature.ProvinceAndCity(
                                "黑龙江省", "哈尔滨市")),
                Arrays.asList("台球", "游泳"),
                FeatureRelation.OR
        ));
        System.out.println(JSON.toJSONString(request));
        System.out.println(JSON.toJSONString(search.fetchAds(request)));

        // 第二个测试条件
        request.setRequestInfo(new SearchRequest.RequestInfo(
                "aaa",
                Collections.singletonList(new AdSlot(
                        "ad-y", 1,
                        1080, 720, Arrays.asList(1, 2),
                        1000
                )),
                buildExampleApp(),
                buildExampleGeo(),
                buildExampleDevice()
        ));
        request.setFeatureInfo(buildExampleFeatureInfo(
                Arrays.asList("宝马", "大众", "标志"),
                Collections.singletonList(
                        new DistrictFeature.ProvinceAndCity(
                                "河南省", "南阳市")),
                Arrays.asList("台球", "游泳"),
                FeatureRelation.AND
        ));
        System.out.println(JSON.toJSONString(request));
        System.out.println(JSON.toJSONString(search.fetchAds(request)));

    }

    //填充属性，测试用
    private App buildExampleApp() {
        return new App("xiang", "xiang",
                "com.xiang", "video");
    }

    //填充属性，测试用
    private Geo buildExampleGeo() {
        return new Geo((float) 100.28, (float) 88.61,
                "北京市", "北京市");
    }

    //填充属性，测试用
    private Device buildExampleDevice() {

        return new Device(
                "iphone",
                "0xxxxx",
                "127.0.0.1",
                "x",
                "1080 720",
                "1080 720",
                "123456789"

        );
    }

    private SearchRequest.FeatureInfo buildExampleFeatureInfo(
            List<String> keywords,
            List<DistrictFeature.ProvinceAndCity> provinceAndCities,
            List<String> its,
            FeatureRelation relation
    ) {
        return new SearchRequest.FeatureInfo(
                new KeywordFeature(keywords),
                new DistrictFeature(provinceAndCities),
                new ItFeature(its),
                relation
        );
    }
}
