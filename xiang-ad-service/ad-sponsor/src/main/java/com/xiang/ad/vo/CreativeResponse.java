package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by xiang.
 * 创意-响应
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeResponse {
    //只响应主键id 和 创意名称
    private Long id;
    private String name;
}
