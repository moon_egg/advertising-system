package com.xiang.ad.service.impl;

import com.xiang.ad.constant.Constants;
import com.xiang.ad.dao.AdPlanRepository;
import com.xiang.ad.dao.AdUnitRepository;
import com.xiang.ad.dao.CreativeRepository;
import com.xiang.ad.dao.unit_condition.AdUnitDistrictRepository;
import com.xiang.ad.dao.unit_condition.AdUnitItRepository;
import com.xiang.ad.dao.unit_condition.AdUnitKeywordRepository;
import com.xiang.ad.dao.unit_condition.CreativeUnitRepository;
import com.xiang.ad.entity.AdPlan;
import com.xiang.ad.entity.AdUnit;
import com.xiang.ad.entity.unit_condition.AdUnitDistrict;
import com.xiang.ad.entity.unit_condition.AdUnitIt;
import com.xiang.ad.entity.unit_condition.AdUnitKeyword;
import com.xiang.ad.entity.unit_condition.CreativeUnit;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.service.AdUnitService;
import com.xiang.ad.vo.AdUnitDistrictRequest;
import com.xiang.ad.vo.AdUnitDistrictResponse;
import com.xiang.ad.vo.AdUnitItRequest;
import com.xiang.ad.vo.AdUnitItResponse;
import com.xiang.ad.vo.AdUnitKeywordRequest;
import com.xiang.ad.vo.AdUnitKeywordResponse;
import com.xiang.ad.vo.AdUnitRequest;
import com.xiang.ad.vo.AdUnitResponse;
import com.xiang.ad.vo.CreativeUnitRequest;
import com.xiang.ad.vo.CreativeUnitResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by xiang.
 */
@Service
public class AdUnitServiceImpl implements AdUnitService {

    private final AdPlanRepository planRepository;
    private final AdUnitRepository unitRepository;

    private final AdUnitKeywordRepository unitKeywordRepository;
    private final AdUnitItRepository unitItRepository;
    private final AdUnitDistrictRepository unitDistrictRepository;

    private final CreativeRepository creativeRepository;
    private final CreativeUnitRepository creativeUnitRepository;

    @Autowired
    public AdUnitServiceImpl(AdPlanRepository planRepository,
                             AdUnitRepository unitRepository,
                             AdUnitKeywordRepository unitKeywordRepository,
                             AdUnitItRepository unitItRepository,
                             AdUnitDistrictRepository unitDistrictRepository, CreativeRepository creativeRepository, CreativeUnitRepository creativeUnitRepository) {
        this.planRepository = planRepository;
        this.unitRepository = unitRepository;
        this.unitKeywordRepository = unitKeywordRepository;
        this.unitItRepository = unitItRepository;
        this.unitDistrictRepository = unitDistrictRepository;
        this.creativeRepository = creativeRepository;
        this.creativeUnitRepository = creativeUnitRepository;
    }

    //创建推广单元
    @Override
    public AdUnitResponse createUnit(AdUnitRequest request)
            throws AdException {

        //参数校验
        if (!request.createValidate()) {
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //判断 推广计划是否存在
        Optional<AdPlan> adPlan =
                planRepository.findById(request.getPlanId());
        if (!adPlan.isPresent()) {
            throw new AdException(Constants.ErrorMsg.CAN_NOT_FIND_RECORD);
        }
        //如果plan存在，判断 推广单元是否已经存在
        AdUnit oldAdUnit = unitRepository.findByPlanIdAndUnitName(
                request.getPlanId(), request.getUnitName()
        );
        if (oldAdUnit != null) {//存在同名推广单元
            throw new AdException(Constants.ErrorMsg.SAME_NAME_UNIT_ERROR);
        }
        //创建新的 推广单元
        AdUnit newAdUnit = unitRepository.save(
                new AdUnit(request.getPlanId(), request.getUnitName(),
                        request.getPositionType(), request.getBudget())
        );
        //
        return new AdUnitResponse(newAdUnit.getId(),
                newAdUnit.getUnitName());
    }

    //创建 推广单元限制---关键词
    @Override
    public AdUnitKeywordResponse createUnitKeyword(
            AdUnitKeywordRequest request) throws AdException {
        //首先获取传递进来的ids
        List<Long> unitIds = request.getUnitKeywords().stream()
                .map(AdUnitKeywordRequest.UnitKeyword::getUnitId)
                .collect(Collectors.toList());
        //判断是否存在
        if (!isRelatedUnitExist(unitIds)) {
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //ids存在的话，创建emptyList去存储 这些ids
        List<Long> ids = Collections.emptyList();//保存unitKeywords的主键

        //
        List<AdUnitKeyword> unitKeywords = new ArrayList<>();
        if (!CollectionUtils.isEmpty(request.getUnitKeywords())) {
            //循环填充 unitKeywords
            request.getUnitKeywords().forEach(i -> unitKeywords.add(
                    new AdUnitKeyword(i.getUnitId(), i.getKeyword())
            ));
            //保存unitKeywords，接受返回对象ids
            ids = unitKeywordRepository.saveAll(unitKeywords).stream()
                    .map(AdUnitKeyword::getId)
                    .collect(Collectors.toList());
        }

        return new AdUnitKeywordResponse(ids);
    }

    //创建 推广单元限制---兴趣
    @Override
    public AdUnitItResponse createUnitIt(
            AdUnitItRequest request) throws AdException {
        //确保关联的推广单元是存在的
        List<Long> unitIds = request.getUnitIts().stream()
                .map(AdUnitItRequest.UnitIt::getUnitId)
                .collect(Collectors.toList());//获取所有的unitIds
        if (!isRelatedUnitExist(unitIds)) {//判断是否存在
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //遍历填充unitIts
        List<AdUnitIt> unitIts = new ArrayList<>();
        request.getUnitIts().forEach(i -> unitIts.add( //遍历填充it的unit
                new AdUnitIt(i.getUnitId(), i.getItTag())
        ));
        //保存 兴趣的推广单元
        List<Long> ids = unitItRepository.saveAll(unitIts).stream()
                .map(AdUnitIt::getId)
                .collect(Collectors.toList());

        return new AdUnitItResponse(ids);
    }

    //创建 推广单元限制---地域
    @Override
    public AdUnitDistrictResponse createUnitDistrict(
            AdUnitDistrictRequest request) throws AdException {
        //确保关联的推广单元是存在的
        List<Long> unitIds = request.getUnitDistricts().stream()
                .map(AdUnitDistrictRequest.UnitDistrict::getUnitId)
                .collect(Collectors.toList());
        if (!isRelatedUnitExist(unitIds)) {
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //如果传递进来的ids是存在的，进行兴趣限制的创建和保存
        List<AdUnitDistrict> unitDistricts = new ArrayList<>();
        request.getUnitDistricts().forEach(d -> unitDistricts.add(
                new AdUnitDistrict(d.getUnitId(), d.getProvince(),
                        d.getCity())
        ));
        //保存
        List<Long> ids = unitDistrictRepository.saveAll(unitDistricts)
                .stream().map(AdUnitDistrict::getId)
                .collect(Collectors.toList());

        return new AdUnitDistrictResponse(ids);
    }

    //  推广单元&创意关联    传递进来的 unitId 和 creativeId 需要进行校验
    @Override
    public CreativeUnitResponse createCreativeUnit(
            CreativeUnitRequest request) throws AdException {

        //获取unitId 和 creativeId
        List<Long> unitIds = request.getUnitItems().stream()
                .map(CreativeUnitRequest.CreativeUnitItem::getUnitId)
                .collect(Collectors.toList());
        List<Long> creativeIds = request.getUnitItems().stream()
                .map(CreativeUnitRequest.CreativeUnitItem::getCreativeId)
                .collect(Collectors.toList());
        // 校验unitId 和 creativeId 是否存在
        if (!(isRelatedUnitExist(unitIds) && isRelatedCreativeExist(creativeIds))) {
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }

        //创建creativeUnits 映射关系
        List<CreativeUnit> creativeUnits = new ArrayList<>();
        request.getUnitItems().forEach(i -> creativeUnits.add(
                new CreativeUnit(i.getCreativeId(), i.getUnitId())
        ));

        //保存 creativeUnits 映射关系，并接受返回值
        List<Long> ids = creativeUnitRepository.saveAll(creativeUnits)
                .stream()
                .map(CreativeUnit::getId)
                .collect(Collectors.toList());
        //返回response
        return new CreativeUnitResponse(ids);
    }


    //================================校验函数========================================
    //校验 传递进来的unit 的 ids 是否存在
    private boolean isRelatedUnitExist(List<Long> unitIds) {
        //如果传递进来的ids为空
        if (CollectionUtils.isEmpty(unitIds)) {
            return false;
        }
        //传递进来的有可能会重复，用Hashset去重
        return unitRepository.findAllById(unitIds).size() == new HashSet<>(unitIds).size();
    }

    //校验 传递进来的create 的 ids 是否存在
    private boolean isRelatedCreativeExist(List<Long> creativeIds) {

        if (CollectionUtils.isEmpty(creativeIds)) {
            return false;
        }

        //传递进来的有可能会重复，用Hashset去重
        return creativeRepository.findAllById(creativeIds).size() ==
                new HashSet<>(creativeIds).size();
    }
}
