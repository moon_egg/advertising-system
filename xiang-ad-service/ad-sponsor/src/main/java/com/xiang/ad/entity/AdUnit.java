package com.xiang.ad.entity;

import com.xiang.ad.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by xiang.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity  // 标记为实体类
@Table(name = "ad_unit") // 推广单元-依赖于推广计划
public class AdUnit {

    @Id // 主键
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 主键自增
    @Column(name = "id", nullable = false)
    private Long id;


    // 推广计划的关联字段
    @Basic
    @Column(name = "plan_id", nullable = false)
    private Long planId;

    @Basic
    @Column(name = "unit_name", nullable = false) //推广单元名称
    private String unitName;

    @Basic
    @Column(name = "unit_status", nullable = false) // 状态，标记是否有效，也是两种状态1 0
    private Integer unitStatus;

    /** 广告位类型(开屏, 贴片, 中贴,暂停贴，后贴...) */
    @Basic
    @Column(name = "position_type", nullable = false) // 广告位的类型，用数字标识
    private Integer positionType;

    @Basic
    @Column(name = "budget", nullable = false) // 预算
    private Long budget;

    @Basic
    @Column(name = "create_time", nullable = false)// 创建时间
    private Date createTime;

    @Basic
    @Column(name = "update_time", nullable = false) // 更新时间
    private Date updateTime;

    // 推广单元 所需 一些字段的 构造函数
    public AdUnit(Long planId, String unitName,
                  Integer positionType, Long budget) {
        this.planId = planId;
        this.unitName = unitName;
        this.unitStatus = CommonStatus.VALID.getStatus();
        this.positionType = positionType;
        this.budget = budget;
        this.createTime = new Date();
        this.updateTime = this.createTime;
    }
}
