package com.xiang.ad.service;

import com.xiang.ad.entity.AdPlan;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.vo.AdPlanGetRequest;
import com.xiang.ad.vo.AdPlanRequest;
import com.xiang.ad.vo.AdPlanResponse;

import java.util.List;

/**
 * Created by xiang.
 * 推广计划的 service  crud
 */
public interface AdPlanService {

    /**
     * 创建 推广计划
     * */
    AdPlanResponse createAdPlan(AdPlanRequest request) throws AdException;

    /**
     * 获取 推广计划（批量）
     * */
    List<AdPlan> getAdPlanByIds(AdPlanGetRequest request) throws AdException;

    /**
     * 更新 推广计划
     * */
    AdPlanResponse updateAdPlan(AdPlanRequest request) throws AdException;

    /**
     * 删除 推广计划
     * */
    void deleteAdPlan(AdPlanRequest request) throws AdException;
}
