package com.xiang.ad.service;

import com.xiang.ad.exception.AdException;
import com.xiang.ad.vo.AdUnitDistrictRequest;
import com.xiang.ad.vo.AdUnitDistrictResponse;
import com.xiang.ad.vo.AdUnitItRequest;
import com.xiang.ad.vo.AdUnitItResponse;
import com.xiang.ad.vo.AdUnitKeywordRequest;
import com.xiang.ad.vo.AdUnitKeywordResponse;
import com.xiang.ad.vo.AdUnitRequest;
import com.xiang.ad.vo.AdUnitResponse;
import com.xiang.ad.vo.CreativeUnitRequest;
import com.xiang.ad.vo.CreativeUnitResponse;

/**
 * Created by xiang.
 */
public interface AdUnitService {

    //创建unit
    AdUnitResponse createUnit(AdUnitRequest request) throws AdException;

    //===============================
    //创建单元限制-关键字
    AdUnitKeywordResponse createUnitKeyword(AdUnitKeywordRequest request)
        throws AdException;
    //创建单元限制-兴趣
    AdUnitItResponse createUnitIt(AdUnitItRequest request)
        throws AdException;
    //创建单元限制-地域
    AdUnitDistrictResponse createUnitDistrict(AdUnitDistrictRequest request)
        throws AdException;
    //===============================

    //  单元&创意 关联  将 推广单元 与 创意 服务的接口也定义在推广单元中
    CreativeUnitResponse createCreativeUnit(CreativeUnitRequest request)
        throws AdException;
}
