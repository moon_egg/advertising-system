package com.xiang.ad.constant;

import lombok.Getter;

/**
 * Created by xiang.
 */
@Getter   // lombok注解，可以主动获取属性
public enum CommonStatus {

    VALID(1, "有效状态"),
    INVALID(0, "无效状态");

    private Integer status; // 状态码
    private String desc; // 描述信息
    // 构造函数
    CommonStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }
}
