package com.xiang.ad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created by xiang.
 */
@EnableFeignClients  // 启动这个注解，这个微服务中就可以调用其他的微服务，项目中没涉及其他的调用，只是为了dashboad监控用
@EnableCircuitBreaker //断路器，也是为了实现监控
@EnableEurekaClient // 标识这是一个 eureka client ，能够从注册中心中拿取到其他的一些微服务的相关信息
@SpringBootApplication
// 主启动类
public class SponsorApplication {

    public static void main(String[] args) {

        SpringApplication.run(SponsorApplication.class, args);
    }
}
