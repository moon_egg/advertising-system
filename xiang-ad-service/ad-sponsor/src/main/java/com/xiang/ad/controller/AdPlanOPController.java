package com.xiang.ad.controller;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.entity.AdPlan;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.service.AdPlanService;
import com.xiang.ad.vo.AdPlanGetRequest;
import com.xiang.ad.vo.AdPlanRequest;
import com.xiang.ad.vo.AdPlanResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by xiang.
 * 推广计划的controller    create、get、update、delete
 */
@Slf4j
@RestController
public class AdPlanOPController {

    //注入 推广计划的service接口
    private final AdPlanService adPlanService;

    @Autowired
    public AdPlanOPController(AdPlanService adPlanService) {
        this.adPlanService = adPlanService;
    }

    //创建 推广计划
    @PostMapping("/create/adPlan")
    public AdPlanResponse createAdPlan(
            @RequestBody AdPlanRequest request) throws AdException {
        log.info("ad-sponsor: createAdPlan -> {}",
                JSON.toJSONString(request));
        //
        return adPlanService.createAdPlan(request);
    }

    //查询 推广计划
    @PostMapping("/get/adPlan")
    public List<AdPlan> getAdPlanByIds(
            @RequestBody AdPlanGetRequest request) throws AdException {
        log.info("ad-sponsor: getAdPlanByIds -> {}",
                JSON.toJSONString(request));
        //
        return adPlanService.getAdPlanByIds(request);
    }

    //更新 推广计划
    @PutMapping("/update/adPlan")    //因为是一个更新的请求，根据restful标准，使用putmapping
    public AdPlanResponse updateAdPlan(
            @RequestBody AdPlanRequest request) throws AdException {
        log.info("ad-sponsor: updateAdPlan -> {}",
                JSON.toJSONString(request));
        //
        return adPlanService.updateAdPlan(request);
    }

    //删除 推广计划
    @DeleteMapping("/delete/adPlan")
    public void deleteAdPlan(
            @RequestBody AdPlanRequest request) throws AdException {
        log.info("ad-sponsor: deleteAdPlan -> {}",
                JSON.toJSONString(request));
        //
        adPlanService.deleteAdPlan(request);
    }
}
