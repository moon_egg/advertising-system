package com.xiang.ad.dao.unit_condition;

import com.xiang.ad.entity.unit_condition.AdUnitKeyword;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by xiang.
 */
// JpaRepository<实体类的class定义,主键类型>
public interface AdUnitKeywordRepository extends JpaRepository<AdUnitKeyword, Long> {

    // 根据扩展再添加方法

}
