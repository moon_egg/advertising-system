package com.xiang.ad.dao;

import com.xiang.ad.entity.AdPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by xiang.
 */
// JpaRepository<实体类的class定义,主键类型>
public interface AdPlanRepository extends JpaRepository<AdPlan, Long> {

    // 根据 主键 以及 userId（跟user关联）查询一个plan
    AdPlan findByIdAndUserId(Long id, Long userId); // userId做一个限制，更加准确的寻找AdPlan
    // 通过列表的方式查询多个 adplan
    List<AdPlan> findAllByIdInAndUserId(List<Long> ids, Long userId);

    //根据 userid 和 planname 查询 adplan
    AdPlan findByUserIdAndPlanName(Long userId, String planName);
    // 通过列表方式 查询多个adplan
    List<AdPlan> findAllByPlanStatus(Integer status);
}
