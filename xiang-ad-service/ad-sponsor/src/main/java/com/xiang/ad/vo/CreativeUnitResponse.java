package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 创意 与 推广单元 关联
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeUnitResponse {
    //只返回 两者之间 数据表的主键
    private List<Long> ids;
}
