package com.xiang.ad.dao;

import com.xiang.ad.entity.Creative;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by xiang.
 */
// JpaRepository<实体类的class定义,主键类型>
public interface CreativeRepository extends JpaRepository<Creative, Long> {

    //不写任何方法的话，基本的 findby、save(update)、delete 之类的都可以直接使用
}
