package com.xiang.ad.entity.unit_condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by xiang.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "creative_unit") //创意-推广单元关联表，维系创意和推广单元之间的关系
public class CreativeUnit {

    // 主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    // 维系创意和推广单元之间的关系 需要 创意id 和 推广单元id
    @Basic
    @Column(name = "creative_id", nullable = false) // 创意id，关联创意
    private Long creativeId;

    @Basic
    @Column(name = "unit_id", nullable = false) // 推广单元id，关联推广单元
    private Long unitId;

    // 构造函数
    public CreativeUnit(Long creativeId, Long unitId) {
        this.creativeId = creativeId;
        this.unitId = unitId;
    }
}
