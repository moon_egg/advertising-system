package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by xiang.
 * 获取当前系统中的推广计划
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdPlanGetRequest {

    private Long userId; //根据userId定位adPlan
    private List<Long> ids; //支持批量获取，通过多个plan的id获取多个推广计划的详细信息

    //参数校验，这里只有简单的非空校验
    public boolean validate() {
        return userId != null && !CollectionUtils.isEmpty(ids);
    }
}
