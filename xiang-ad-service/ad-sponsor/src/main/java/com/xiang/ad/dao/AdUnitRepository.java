package com.xiang.ad.dao;

import com.xiang.ad.entity.AdUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by xiang.
 */
// JpaRepository<实体类的class定义,主键类型>
public interface AdUnitRepository extends JpaRepository<AdUnit, Long> {

    // 根据planId和UnitName唯一的确定一个推广单元
    AdUnit findByPlanIdAndUnitName(Long planId, String unitName);
    // 查询多个推广单元
    List<AdUnit> findAllByUnitStatus(Integer unitStatus);
}
