package com.xiang.ad.service;

import com.xiang.ad.vo.CreativeRequest;
import com.xiang.ad.vo.CreativeResponse;

/**
 * Created by xiang.
 * 创意 的 service
 */
public interface CreativeService {

    CreativeResponse createCreative(CreativeRequest request);
}
