package com.xiang.ad.vo;

import com.xiang.ad.constant.CommonStatus;
import com.xiang.ad.entity.Creative;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by xiang.
 * 创意-请求
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeRequest {

    private String name; //创意名称
    private Integer type; //创意主类型，图片、视频、文本
    private Integer materialType; //创意的子类型，物料类型
    private Integer height;//高度
    private Integer width;//宽度
    private Long size;//物料的大小
    private Integer duration;//持续的时长
    private Long userId;//所属用户id
    private String url;//url

    //实现将 request 转化为 Creative实体类对象
    public Creative convertToEntity() {

        Creative creative = new Creative();
        creative.setName(name);
        creative.setType(type);
        creative.setMaterialType(materialType);
        creative.setHeight(height);
        creative.setWidth(width);
        creative.setSize(size);
        creative.setDuration(duration);
        creative.setAuditStatus(CommonStatus.VALID.getStatus());//审核状态，设为合格的
        creative.setUserId(userId);
        creative.setUrl(url);

        creative.setCreateTime(new Date());//创建时间
        creative.setUpdateTime(creative.getCreateTime());//更新时间，创建这个时候与创建时间保持一致
        //返还一个 creative
        return creative;
    }
}
