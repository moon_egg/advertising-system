package com.xiang.ad.controller;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.service.UserService;
import com.xiang.ad.vo.CreateUserRequest;
import com.xiang.ad.vo.CreateUserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xiang.
 * User的 controller  目前只有创建用户
 */
@Slf4j
@RestController
public class UserOPController {

    //注入userService
    private final UserService userService;

    @Autowired
    public UserOPController(UserService userService) {
        this.userService = userService;
    }

    //创建用户
    @PostMapping("/create/user")
    public CreateUserResponse createUser(
            @RequestBody CreateUserRequest request) throws AdException {
        log.info("ad-sponsor: createUser -> {}",//打印日志
                JSON.toJSONString(request));//打印request的json

        return userService.createUser(request);
    }
}
