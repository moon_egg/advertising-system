package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 推广单元-兴趣
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitItRequest {

    //允许批量创建
    private List<UnitIt> unitIts;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnitIt {

        private Long unitId;//推广单元的id
        private String itTag;//兴趣标签
    }
}
