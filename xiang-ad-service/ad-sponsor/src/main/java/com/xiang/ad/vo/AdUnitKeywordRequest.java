package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 推广单元-关键词限制
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitKeywordRequest {

    //允许批量创建
    private List<UnitKeyword> unitKeywords;

    //使用内部类
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnitKeyword {

        private Long unitId; //关联的推广单元
        private String keyword;// 推广单元所限制的关键字
    }
}
