package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

/**
 * Created by xiang.
 */

//创建用户的请求 需要的数据
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {

    private String username;//创建的时候只需要提供username，token在后台主动生成

    //验证参数的有效性
    public boolean validate() {

        return !StringUtils.isEmpty(username);//验证username是否为空，非空为true
    }
}
