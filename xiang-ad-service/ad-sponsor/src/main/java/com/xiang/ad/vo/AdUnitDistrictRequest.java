package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 兴趣单元-地域限制
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitDistrictRequest {

    //允许批量创建，这里可以扩展，标准的做法应该是有一份地域字典
    private List<UnitDistrict> unitDistricts;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnitDistrict {

        private Long unitId;//关联推广单元id
        private String province;//省
        private String city;//市
    }
}
