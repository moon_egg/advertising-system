package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

/**
 * Created by xiang.
 * 推广单元 请求
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitRequest {

    private Long planId; //关联 推广计划
    private String unitName;//推广单元名字

    private Integer positionType;//广告的类型，进行切片
    private Long budget;//推广单元的预算

    //参数校验
    public boolean createValidate() {

        return null != planId && !StringUtils.isEmpty(unitName)
                && positionType != null && budget != null;
    }
}
