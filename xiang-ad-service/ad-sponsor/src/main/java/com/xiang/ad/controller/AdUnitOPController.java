package com.xiang.ad.controller;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.service.AdUnitService;
import com.xiang.ad.vo.AdUnitDistrictRequest;
import com.xiang.ad.vo.AdUnitDistrictResponse;
import com.xiang.ad.vo.AdUnitItRequest;
import com.xiang.ad.vo.AdUnitItResponse;
import com.xiang.ad.vo.AdUnitKeywordRequest;
import com.xiang.ad.vo.AdUnitKeywordResponse;
import com.xiang.ad.vo.AdUnitRequest;
import com.xiang.ad.vo.AdUnitResponse;
import com.xiang.ad.vo.CreativeUnitRequest;
import com.xiang.ad.vo.CreativeUnitResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xiang.
 * 推广单元 及 推广单元相关的 controller
 * create 推广单元的创建
 * create 推广单元的限制-关键词、兴趣、地域
 * 推广单元 与 创意 的映射关系
 */
@Slf4j
@RestController
public class AdUnitOPController {

    //注入推广单元的service
    private final AdUnitService adUnitService;

    @Autowired
    public AdUnitOPController(AdUnitService adUnitService) {
        this.adUnitService = adUnitService;
    }


    //创建 推广单元
    @PostMapping("/create/adUnit")
    public AdUnitResponse createUnit(
            @RequestBody AdUnitRequest request) throws AdException {
        log.info("ad-sponsor: createUnit -> {}",
                JSON.toJSONString(request));
        //
        return adUnitService.createUnit(request);
    }

    //创建 推广单元限制---关键词
    @PostMapping("/create/unitKeyword")
    public AdUnitKeywordResponse createUnitKeyword(
            @RequestBody AdUnitKeywordRequest request
            ) throws AdException {
        log.info("ad-sponsor: createUnitKeyword -> {}",
                JSON.toJSONString(request));
        //
        return adUnitService.createUnitKeyword(request);
    }

    //创建 推广单元限制---兴趣
    @PostMapping("/create/unitIt")
    public AdUnitItResponse createUnitIt(
            @RequestBody AdUnitItRequest request
            ) throws AdException {
        log.info("ad-sponsor: createUnitIt -> {}",
                JSON.toJSONString(request));
        //
        return adUnitService.createUnitIt(request);
    }

    //创建 推广单元限制---地域
    @PostMapping("/create/unitDistrict")
    public AdUnitDistrictResponse createUnitDistrict(
            @RequestBody AdUnitDistrictRequest request
            ) throws AdException {
        log.info("ad-sponsor: createUnitDistrict -> {}",
                JSON.toJSONString(request));
        return adUnitService.createUnitDistrict(request);
    }

    //创建 unit与creative的映射关系
    @PostMapping("/create/creativeUnit")
    public CreativeUnitResponse createCreativeUnit(
            @RequestBody CreativeUnitRequest request
            ) throws AdException {
        log.info("ad-sponsor: createCreativeUnit -> {}",
                JSON.toJSONString(request));
        //
        return adUnitService.createCreativeUnit(request);
    }
}
