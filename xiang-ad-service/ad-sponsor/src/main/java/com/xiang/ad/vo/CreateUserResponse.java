package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by xiang.
 */

//创建完用户之后，给广告主的响应
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserResponse {

    private Long userId;   //用户的id
    private String username;//创建时候使用的 username
    private String token; //后台生成的token
    private Date createTime; // 创建时间
    private Date updateTime; // 更新时间
}
