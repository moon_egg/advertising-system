package com.xiang.ad.dao.unit_condition;

import com.xiang.ad.entity.unit_condition.AdUnitIt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by xiang.
 */
// JpaRepository<实体类的class定义,主键类型>
public interface AdUnitItRepository
        extends JpaRepository<AdUnitIt, Long> {
}
