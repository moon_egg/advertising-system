package com.xiang.ad.constant;

import lombok.Getter;

/**
 * Created by xiang.
 */
@Getter
public enum CreativeType { //创意的类型

    IMAGE(1, "图片"),
    VIDEO(2, "视频"),
    TEXT(3, "文本");

    private int type; //类型
    private String desc; //描述信息

    CreativeType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
