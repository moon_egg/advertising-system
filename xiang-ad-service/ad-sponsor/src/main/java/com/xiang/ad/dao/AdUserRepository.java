package com.xiang.ad.dao;

import com.xiang.ad.entity.AdUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by xiang.
 */

// JpaRepository<实体类的class定义,主键类型>
public interface AdUserRepository extends JpaRepository<AdUser, Long> {

    // 根据用户名查找用户记录
    AdUser findByUsername(String username);
}
