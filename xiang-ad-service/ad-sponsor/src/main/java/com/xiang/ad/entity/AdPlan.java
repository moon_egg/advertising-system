package com.xiang.ad.entity;

import com.xiang.ad.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by xiang.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity // 标记为实体类
@Table(name = "ad_plan")   // 推广计划
public class AdPlan {

    @Id // 主键
    @GeneratedValue(strategy = GenerationType.IDENTITY) //自增策略
    @Column(name = "id", nullable = false)
    private Long id;


    /** 这里关联关系的定义 而不是选择 外键的定义的原因：
     * 1、企业级开发中不允许使用外键（阿里巴巴开发手册可以查到）
     * 2、外键占用空间，
     * 3、外键和母标之间有关联，母表一旦损坏，子表很难恢复
     * 4、在数据表的迁移和分库分表时候有外键的情况下不好操作，也不好维护
     * 5、一般建议在应用程序中维持外键的关系，而不是在定义表的时候
    */
    @Basic
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Basic
    @Column(name = "plan_name", nullable = false) // 推广计划 名称
    private String planName;

    @Basic
    @Column(name = "plan_status", nullable = false) //推广计划 状态
    private Integer planStatus;

    @Basic
    @Column(name = "start_date", nullable = false)// 推广计划开始的时间
    private Date startDate;

    @Basic
    @Column(name = "end_date", nullable = false) // 推广计划结束的时间
    private Date endDate;

    @Basic
    @Column(name = "create_time", nullable = false) // 创建时间
    private Date createTime;

    @Basic
    @Column(name = "update_time", nullable = false) // 更新时间
    private Date updateTime;

    // 构造函数，哪些属性需要初始化，就在这里处理
    public AdPlan(Long userId, String planName,
                  Date startDate, Date endDate) {

        this.userId = userId;
        this.planName = planName;

        this.startDate = startDate;
        this.endDate = endDate;

        //
        this.planStatus = CommonStatus.VALID.getStatus(); // 推广计划创建的时候 默认类型
        this.createTime = new Date();//
        this.updateTime = this.createTime;
    }
}
