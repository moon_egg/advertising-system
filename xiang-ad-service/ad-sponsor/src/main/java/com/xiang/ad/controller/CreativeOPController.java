package com.xiang.ad.controller;

import com.alibaba.fastjson.JSON;
import com.xiang.ad.service.CreativeService;
import com.xiang.ad.vo.CreativeRequest;
import com.xiang.ad.vo.CreativeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xiang.
 * 创意相关的 controller  目前只有创建
 */
@Slf4j
@RestController
public class CreativeOPController {

    //注入creative的service接口
    private final CreativeService creativeService;

    @Autowired
    public CreativeOPController(CreativeService creativeService) {
        this.creativeService = creativeService;
    }

    //创建 creative
    @PostMapping("/create/creative")
    public CreativeResponse createCreative(
            @RequestBody CreativeRequest request
            ) {
        log.info("ad-sponsor: createCreative -> {}",
                JSON.toJSONString(request));
        //
        return creativeService.createCreative(request);
    }
}
