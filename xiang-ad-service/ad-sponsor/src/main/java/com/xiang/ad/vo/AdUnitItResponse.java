package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by xiang.
 * 推广单元-兴趣
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdUnitItResponse {

    //批量
    private List<Long> ids;
}
