package com.xiang.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

/**
 * Created by xiang.
 * 与response搭配，为之后的update操作提供服务
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdPlanRequest {

    private Long id;// 这个id只是为了更新的时候用

    private Long userId; //标识plan属于哪个用户
    private String planName; // plan的name

    //这里用string，需要做反序列化将字符串类型的data转化成java.util.DATE类型
    private String startDate; //
    private String endDate; //

    //创建时候的参数校验，这里只有非空校验
    public boolean createValidate() {

        return userId != null
                && !StringUtils.isEmpty(planName)
                && !StringUtils.isEmpty(startDate)
                && !StringUtils.isEmpty(endDate);
    }
    //更新操作时候的参数校验
    public boolean updateValidate() {

        return id != null && userId != null;
    }

    //删除操作时候的参数校验
    public boolean deleteValidate() {

        return id != null && userId != null;
    }
}
