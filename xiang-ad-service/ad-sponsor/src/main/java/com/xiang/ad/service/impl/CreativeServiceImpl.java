package com.xiang.ad.service.impl;

import com.xiang.ad.dao.CreativeRepository;
import com.xiang.ad.entity.Creative;
import com.xiang.ad.service.CreativeService;
import com.xiang.ad.vo.CreativeRequest;
import com.xiang.ad.vo.CreativeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by xiang.
 * creativeservice的impl
 */
@Service
public class CreativeServiceImpl implements CreativeService {

    //引入creative dao接口
    private final CreativeRepository creativeRepository;

    @Autowired
    public CreativeServiceImpl(CreativeRepository creativeRepository) {
        this.creativeRepository = creativeRepository;
    }

    //创建创意    这里不做校验了，之前有很多相关的校验工作了
    //这里只做简单的保存，可以继续扩展校验，比如传递参数是否有误，以及传递的userid是否对应存在
    @Override
    public CreativeResponse createCreative(CreativeRequest request) {

        // 保存创意 使用creative的dao接口 将request对象转化为creative实体类对象
        Creative creative = creativeRepository.save(
                request.convertToEntity()
        );

        //响应
        return new CreativeResponse(creative.getId(), creative.getName());
    }
}
