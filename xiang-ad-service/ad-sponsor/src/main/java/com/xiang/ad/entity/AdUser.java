package com.xiang.ad.entity;

import com.xiang.ad.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by xiang.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity  //标记为实体类
@Table(name = "ad_user")
public class AdUser {

    @Id  //主键用id注解去标识
    //主键生成策略选择：使用数据库的自增策略去完成
    @GeneratedValue(strategy = GenerationType.IDENTITY) //
    @Column(name = "id", nullable = false)  //对应到数据库的字段名字，不允许为null
    private Long id;

    @Basic // basic注解标识 是一个基本属性，写不写默认都是basic，可以不写
    @Column(name = "username", nullable = false)
    private String username;

    @Basic
    @Column(name = "token", nullable = false)
    private String token;

    @Basic
    @Column(name = "user_status", nullable = false)
    // 这个用户创建的时候就是一个有效的状态
    private Integer userStatus;

    @Basic
    @Column(name = "create_time", nullable = false)
    // 生成时间，用的是“当前时间”，不需要指定
    private Date createTime;

    @Basic
    @Column(name = "update_time", nullable = false)
    // 更新时间，也用的“当前时间”，不需要指定
    private Date updateTime;

    // 单独定义一个构造函数，将来用户的创建使用，只需要给出需要的字段就行了，时间这个不需要
    public AdUser(String username, String token) {


        this.username = username;
        this.token = token;

        this.userStatus = CommonStatus.VALID.getStatus(); //用户状态，这里创建时候设为有效
        this.createTime = new Date(); //用户 创建时间，选择为当前时间
        this.updateTime = this.createTime; // 用户创建的时候，更新时间应该与创建时间一致
    }
}
