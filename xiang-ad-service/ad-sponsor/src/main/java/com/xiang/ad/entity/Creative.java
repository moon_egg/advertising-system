package com.xiang.ad.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by xiang.
 * 创意，最终展现给用户的看的一些广告数据，广告请求返回的数据就叫做创意。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity //标识为 实体类
@Table(name = "ad_creative")  // 创意本身不和任何的实体类相关联
public class Creative {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name", nullable = false) // 创意名称
    private String name;

    @Basic
    @Column(name = "type", nullable = false) // 创意类型（主类型），标识了创意是什么，图片、视频还是文本
    private Integer type;

    /** 物料的类型, 比如图片可以是 png，bmp, jpg等等，我们去渲染广告的话，需要知道物料的类型 */
    @Basic
    @Column(name = "material_type", nullable = false)
    private Integer materialType;

    @Basic
    @Column(name = "height", nullable = false)
    private Integer height;

    @Basic
    @Column(name = "width", nullable = false)
    private Integer width;

    /** 物料大小 */
    @Basic
    @Column(name = "size", nullable = false)
    private Long size;

    /** 持续时长, 只有视频不为0 */
    @Basic
    @Column(name = "duration", nullable = false)
    private Integer duration;

    /** 审核状态 */
    @Basic
    @Column(name = "audit_status", nullable = false)
    private Integer auditStatus;

    @Basic
    @Column(name = "user_id", nullable = false) // 每一个物料都是用户上传的
    private Long userId;

    @Basic
    @Column(name = "url", nullable = false) // 物料的地址信息，存储在某些地方 云什么的都行，最终也是返回给广告请求方。
    private String url;

    @Basic
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    @Basic
    @Column(name = "updateTime", nullable = false)
    private Date updateTime;
}
