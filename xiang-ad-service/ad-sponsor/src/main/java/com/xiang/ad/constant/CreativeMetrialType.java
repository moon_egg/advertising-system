package com.xiang.ad.constant;

import lombok.Getter;

/**
 * Created by xiang.
 */
@Getter
public enum CreativeMetrialType { //创意-物料类型

    // 图片的子类型
    JPG(1, "jpg"),
    BMP(2, "bmp"),

    //视频的子类型
    MP4(3, "mp4"),
    AVI(4, "avi"),

    // 文本的子类型
    TXT(5, "txt");

    private int type;
    private String desc;

    CreativeMetrialType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
