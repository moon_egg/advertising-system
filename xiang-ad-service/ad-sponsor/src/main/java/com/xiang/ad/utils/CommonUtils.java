package com.xiang.ad.utils;

import com.xiang.ad.exception.AdException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.time.DateUtils;

import java.util.Date;

/**
 * Created by xiang.
 * 通过工具类，目前有：
 * 1、m5生成token
 * 2、
 */
public class CommonUtils {

    //定义字符串的格式
    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy/MM/dd", "yyyy.MM.dd" //只要满足一个格式 就可以转换
    };

    //m5生成token
    public static String md5(String value) {

        return DigestUtils.md5Hex(value).toUpperCase();
    }

    //转化string为DATE类型
    public static Date parseStringDate(String dateString)
            throws AdException {

        try {
            return DateUtils.parseDate( //DateUtils  apache的工具类
                    dateString, parsePatterns
            );
        } catch (Exception ex) {
            throw new AdException(ex.getMessage());
        }
    }
}
