package com.xiang.ad.service;

import com.xiang.ad.exception.AdException;
import com.xiang.ad.vo.CreateUserRequest;
import com.xiang.ad.vo.CreateUserResponse;

/**
 * Created by xiang.
 */
public interface UserService {

    /**
     * 创建用户
     * */
    CreateUserResponse createUser(CreateUserRequest request)
            throws AdException;
}
