package com.xiang.ad.service;

import com.xiang.ad.Application;
import com.xiang.ad.exception.AdException;
import com.xiang.ad.vo.AdPlanGetRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

/**
 * Created by xiang.
 * service的 可行性测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class AdPlanServiceTest {

    @Autowired
    private AdPlanService planService;

    //测试下 get推广计划
    @Test
    public void testGetAdPlan() throws AdException {

        System.out.println(
                planService.getAdPlanByIds(
                        new AdPlanGetRequest(15L, Collections.singletonList(10L))
                )
        );
    }
}
/**
 * 测试结果：
 * Hibernate:
 *     select
 *         adplan0_.id as id1_1_,
 *         adplan0_.create_time as create_t2_1_,
 *         adplan0_.end_date as end_date3_1_,
 *         adplan0_.plan_name as plan_nam4_1_,
 *         adplan0_.plan_status as plan_sta5_1_,
 *         adplan0_.start_date as start_da6_1_,
 *         adplan0_.update_time as update_t7_1_,
 *         adplan0_.user_id as user_id8_1_
 *     from
 *         ad_plan adplan0_
 *     where
 *         (
 *             adplan0_.id in (
 *                 ?
 *             )
 *         )
 *         and adplan0_.user_id=?
 * [AdPlan(id=10, userId=15, planName=推广计划名称, planStatus=1, startDate=2021-11-28 08:00:00.0, endDate=2021-11-20 08:00:00.0, createTime=2021-11-20 04:42:27.0, updateTime=2021-11-20 04:57:12.0)]
 */