package com.xiang.ad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by xiang.
 * 测试用例启动文件（导出mysql数据到文件）
 */
@SpringBootApplication  //当做一个springboot应用
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
