package com.xiang.ad.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

/**
 * Created by xiang.
 */
@Slf4j
@Component
public class PreRequestFilter extends ZuulFilter {
    // 定义filter类型  pre post error  routing
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    } //标识为pre类型
    // 定义filter执行的顺序，越小执行优先级越高
    @Override
    public int filterOrder() {
        return 0;
    }
    // 表示是否执行这个过滤器，默认是false
    // 因为可能存在一些验证，在某种情况下才 执行过滤器
    @Override
    public boolean shouldFilter() {
        return true;
    }  //永远执行
    //
    @Override
    public Object run() throws ZuulException {

        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.set("startTime", System.currentTimeMillis());// 记录当前的时间戳，存入context上下文

        return null;
    }
}
