
--
-- init data for table `ad_user`
--

INSERT INTO `ad_user` VALUES (15,'xiang','B2E56F2420D73FEC125D2D51641C5713',1,'2021-11-19 20:29:01','2021-11-19 20:29:01');

--
-- init data for table `ad_creative`
--

INSERT INTO `ad_creative` VALUES (10,'第一个创意',1,1,720,1080,1024,0,1,15,'https://www.xiang.com','2021-11-19 21:31:31','2021-11-19 21:31:31');

--
-- init data for table `ad_plan`
--

INSERT INTO `ad_plan` VALUES (10,15,'推广计划名称',1,'2021-11-28 00:00:00','2021-11-20 00:00:00','2021-11-19 20:42:27','2021-11-19 20:57:12');

--
-- init data for table `ad_unit`
--

INSERT INTO `ad_unit` VALUES (10,10,'第一个推广单元',1,1,10000000,'2021-11-20 11:43:26','2021-11-20 11:43:26'),(12,10,'第二个推广单元',1,1,15000000,'2021-01-01 00:00:00','2021-01-01 00:00:00');

--
-- init data for table `ad_unit_district`
--

INSERT INTO `ad_unit_district` VALUES (10,10,'黑龙江省','哈尔滨市'),(11,10,'吉林省','吉林市'),(12,10,'河南省','南阳市'),(14,10,'辽宁省','大连市');

--
-- init data for table `ad_unit_it`
--

INSERT INTO `ad_unit_it` VALUES (10,10,'台球'),(11,10,'游泳'),(12,10,'乒乓球');

--
-- init data for table `ad_unit_keyword`
--

INSERT INTO `ad_unit_keyword` VALUES (10,10,'宝马'),(11,10,'奥迪'),(12,10,'大众');

--
-- init data for table `creative_unit`
--

INSERT INTO `creative_unit` VALUES (10,10,10);


